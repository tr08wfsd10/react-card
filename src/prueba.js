import React, { Component } from 'react';

class Prueba extends Component{
lista = [
    {
      "id":1,
      "nombre": "Casablanca",
      "bio": "A cynical nightclub owner protects an old flame and her husband from Nazis in Morocco.",
      "img": "public/assets/img/casablanca.jpg",
      "aparicion": "1942"
    },
    {
      "id":2,
      "nombre": "Blade Runner",
      "bio": "A blade runner must pursue and terminate four replicants who stole a ship in space, and have returned to Earth to find their creator.",
      "img": "assets/img/blade_runner.jpg",
      "aparicion": "1982"
    },
    {
      "id":3,
      "nombre": "Golpe en la pequeña China",
      "bio": "A rough-and-tumble trucker helps rescue his friend's fiance from an ancient sorcerer in a supernatural battle beneath Chinatown.",
      "img": "assets/img/golpe_pequena_china.jpg",
      "aparicion": "1986"
    },
    { "id":4,
      "nombre": "Zoolander",
      "bio": "At the end of his career, a clueless fashion model is brainwashed to kill the Prime Minister of Malaysia.",
      "img": "assets/img/zoolander.jpg",
      "aparicion": "2001"
    },
    { "id":5,
      "nombre": "Pesadilla en Elm Street",
      "bio": "The monstrous spirit of a slain janitor seeks revenge by invading the dreams of teenagers whose parents were responsible for his untimely death.",
      "img": "assets/img/pesadilla_elm_street.jpg",
      "aparicion": "1986"
    }
    ]


    constructor(props) {
    super(props);
    this.state = {
    	indice:0	
    }

    this.sumar=this.sumar.bind(this);
  	}
  	sumar(){
  		if (this.state.indice<this.lista.length -1) {
  		this.setState({
  			indice:this.state.indice +1
  		})}else{
  		this.setState({
  			indice:0
  		  	})
  	}
  	}

	render(){
		

	return(

		<div>	

		<h1>{this.lista[this.state.indice].nombre}</h1>
		<img src="{this.lista[this.state.indice].img}"/>
		<p>{this.lista[this.state.indice].aparicion}</p>
		<button onClick={this.sumar} >+</button>

		</div>

	
		
);
}		
	

}

export default Prueba;